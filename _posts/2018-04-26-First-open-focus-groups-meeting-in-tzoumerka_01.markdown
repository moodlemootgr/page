---
layout: post
title:  "First open Focus Group meeting in Kalentzi"
date:   2018-04-26 12:00:00 -0300
location: Kalentzi, Greece
categories: news
featured: '/images/Poster_FocusGroup1_featured.png'
---

An open Focus Group meeting will take place at the cultural center of the Kalentzi municipality on the 26th of April

The topics that will be discussed over the meeting will be Open Technologies on the Primary Sector, the experience gained from the visit to the french team's headquarters ["L'Atelier Paysan"](https://www.latelierpaysan.org/) and the under construction makerspace that is currently being constructed, which is sponsored by the [Phygital Project](http://www.interreg-balkanmed.eu/approved-project/29/).

[Poster (in greek)]({{ "/images/Poster_FocusGroup1.png" | prepend: site.baseurl }})

[Invitation (in greek)]({{ "/assets/files/Invitation_FocusGroup1.pdf" | prepend: site.baseurl }})

[Press release (in greek)]({{ "/assets/files/D3.2.2.prePressRelease_FocusGroup1.pdf" | prepend: site.baseurl }})

[Agenda (in greek)]({{ "/assets/files/Agenda_FocusGrou1.pdf" | prepend: site.baseurl }})