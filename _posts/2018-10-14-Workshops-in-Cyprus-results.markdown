---
layout: post
title:  "Results on the workshops by the University of Nicosia Research Foundation"
date:   2018-10-14 12:00:00 -0300
location: Nicosia, Cyprus
category: news
featured: '/images/1st_UNRF_workshop.png'
---

The debriefing matterial and informational resources from the workshops that were held by the University of Nicosia Research Foundation on October 11th and 13th 2018, regarding the contemporary debates on hackerspaces and their potentialities as spaces of fostering social change and creativity, are available on the links below. 

[Video link](https://youtu.be/oxfBa0NMrIEf)

[Debriefing document]({{ "/assets/files/1st_UNRF_workshop.pdf" | prepend: site.baseurl }})