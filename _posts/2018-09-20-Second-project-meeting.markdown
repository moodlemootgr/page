---
layout: post
title:  "Second Project Meeting "
date:   2018-09-20 12:00:00 -0300
location: Athens, Greece
categories: news
featured: '/images/meeting2.jpg'
---

The Phygital project team was in **Ioannina and Northern Tzoumerka** for a two-day meeting on Thursday 13th and Friday 14th of September 2018. The meeting was organized by the [P2P Lab](http://www.p2plab.gr/) in collaboration with the [Municipality of Northern Tzoumerka](http://www.voreiatzoumerka.gr/) and was the second one for the project, while it was the first one in which the new project partner, the Lakatamia Municipality from Cyprus, participated.

The project partners discussed the most crucial issues of the project for better coordination of the individual actions in the three areas in Greece, Cyprus and Albania. At the same time, they had the opportunity to visit the Makerspace in Kalentzi, in Northern Tzoumerka, which is currently under development, in order to familiarize themselves with the local area and the challenges that the Phygital project aims to address.

[Press release]({{ "/assets/files/PressRelease_2ndProjectMeeting.pdf" | prepend: site.baseurl }})

[Agenda]({{ "/assets/files/2ndProjectMeetingAgenda.pdf" | prepend: site.baseurl }})