---
layout: post
title:  "Workshop by University of Nicosia Research Foundation"
date:   2018-10-10 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/flyer_leandros_workshop_featured.jpg'
excerpt: "Making and connecting in a disconnected world: Hackerspaces as representations of techno-social politics in contemporary capitalist realism. Α Workshop by Leandros Savvides University of Nicosia Research Foundation"
---

**Α**  **Workshop by**  **Leandros Savvides** University of Nicosia Research Foundation


Making and connecting in a disconnected world: Hackerspaces as representations of techno-social politics in contemporary capitalist realism

# University of Nicosia Research Foundation

The workshop will address the emergence of the maker culture, where technology and making are explored outside the professional lab, predominantly in Hackerspaces, Makerspaces and Fab Labs. Such spaces constitute important sites in the development of open-source hardware and software, and provide conducive conditions for the spread of locally created technology to and often beyond technologically informed publics. Specifically, this workshop will address the convergence of activism and the maker culture within capitalist realism. Drawing on the findings of my research, I argue that the politics of Hackerspaces / Makerspaces and Fab Labs constitute sites that disclose contradictions and provide spaces for contesting the dominant paradigm and reflect the societies within which they operate. Users become developers themselves who simultaneously reinvent forms of consumption, processes of learning and re-conceptualizing the relationship between science and craft, productivism and play, authority and informal horizontal learning. Despite the apparent social and collective nature of these practices, there is also a parallel individualistic twist at the heart of the maker culture.

**Presentation and Dialogue-** Thursday, 11 October 2018

18.00-21.00

**Workshop and brainstorming session** - Saturday, 13 October 2018

11.00-14.00

**Lakatamia/ hack66 Community Event** - Saturday, 13 October 2018

14.00 onwards

Location

Makerspace

Lakatamia Polydynamo Centre

Aigaiou, Lakatamia

[https://goo.gl/maps/DPVRyBPUmj72](https://goo.gl/maps/DPVRyBPUmj72)



For more information you can contact

Evanthia Tselika- tselika.e@unic.ac.cy

[![Workshop brochure]({{ "/images/flyer_leandros_workshop.jpg" | prepend: site.baseurl }})]({{ "/images/flyer_leandros_workshop.jpg" | prepend: site.baseurl }})
