---
layout: post
title:  "Results of the second Open Focus Group meeting in Kalentzi"
date:   2018-05-15 12:00:00 -0300
location: Kalentzi, Greece
categories: news
featured: '/images/Poster_FocusGroup2_featured.png'
---

The debriefing documents for the 2nd open Focus Groups meeting that took place yesterday in Kalentzi in collaboration with [P2P Lab](http://www.p2plab.gr/en/) can be found on the following link.

[debriefing (in greek)]({{ "/assets/files/D3.2.2.postPressRelease_FocusGroup2.pdf" | prepend: site.baseurl }})
