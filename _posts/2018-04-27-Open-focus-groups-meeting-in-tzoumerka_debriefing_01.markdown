---
layout: post
title:  "Results of the first Open Focus Group meeting in Kalentzi"
date:   2018-04-27 12:00:00 -0300
location: Kalentzi, Greece
categories: news
featured: '/images/Poster_FocusGroup1_featured.png'
---

The debriefing documentsfor the 1st open Focus Groups meeting that took place yesterday in Kalentzi in collaboration with [P2P Lab](http://www.p2plab.gr/en/) can be found on the following link.

[debriefing (in greek)]({{ "/assets/files/D3.2.2.postPressRelease_FocusGroup1.pdf" | prepend: site.baseurl }})
