---
layout: post
title:  "Mention in the 'Ta nea tis Mikrospilias' blog for the open technologies community in Tzoumerka"
date:   2018-05-24 12:00:00 -0300
location: Thessaloniki, Greece
category: press
---

The ['Ta nea tis Mikrospilias'](http://taneatismikrospilias24.weebly.com) article devoted to the emergence and development of the open technologies community in Tzoumerka. 

[Article link](http://taneatismikrospilias24.weebly.com/alpharhochiiotakappaeta-sigmaepsilonlambdaiotadeltaalpha/tzoumakers)