---
layout: post
title:  "Press coverage for the open technologies community in Tzoumerka"
date:   2018-04-02 12:00:00 -0300
location: Thessaloniki, Greece
category: press
---

The "Antidrastirio" broadcast devoted to the emergence and development of the open technologies community in Tzoumerka. 

[ ERT3 link (video)(in greek)](http://www.ert.gr/ert3/ert3-protaseis/ert3-antidrastirio-tzoumerka-i-genisi-mias-koinotitas-anoihton-tehnologion-trailer/)