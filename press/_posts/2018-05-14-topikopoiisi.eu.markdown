---
layout: post
title:  "Mention in the topikopoiisi.eu blog for the open technologies community in Tzoumerka"
date:   2018-05-14 12:00:00 -0300
location: Thessaloniki, Greece
category: press
---

The [topoikopoiisi.eu](http://www.topikopoiisi.eu/) article devoted to the emergence and development of the open technologies community in Tzoumerka. 

[Article link](http://www.topikopoiisi.eu/902rhothetarhoalpha/-phygital)