---
layout: post
title:  "Mention in the 'Apenanti Oxthi' online newspaper for the open technologies community in Tzoumerka"
date:   2018-05-14 12:00:00 -0300
location: Tzoumerka, Greece
category: press
---

The ['Apenanti Oxthi'](https://www.apenantioxthi.com) article devoted to the emergence and development of the open technologies community in Tzoumerka. 

[Article link](https://www.apenantioxthi.com/2018/04/tzoumerka-h-gennhsh-mias-koinothtas-anoixtwn-texnologiwn.html)